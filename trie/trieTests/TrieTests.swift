//
//  TrieTests.swift
//  trieTests
//
//  Created by Nicholas Trampe on 9/12/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import XCTest
@testable import trie

class TrieTests: XCTestCase {
  
  override func setUp() {
    
  }
  
  override func tearDown() {
    
  }
  
  func test_Contains_GivenWordExists_ThenReturnsTrue() {
    let trie = Trie(["hello"])
    XCTAssertTrue(trie.contains("hello"))
  }
  
  func test_Contains_GivenWordDoesNotExist_ThenReturnsFalse() {
    let trie = Trie(["hello"])
    XCTAssertFalse(trie.contains("world"))
  }
  
  func test_Add_GivenWordAdded_ThenContainsReturnsTrue() {
    var trie = Trie(["hello"])
    trie.add("world")
    XCTAssertTrue(trie.contains("world"))
  }
  
  func test_words_GivenWordsAdded_ThenReturnsWords() {
    let trie = Trie(["hello", "world"])
    XCTAssertEqual(trie.words, ["hello", "world"])
  }
  
  func test_wordStartingWithPrefix_GivenValidPrefix_ThenReturnsWords() {
    let trie = Trie(["hello", "world", "helium"])
    XCTAssertEqual(trie.words(startingWith: "hel"), ["hello", "helium"])
  }
  
  func test_wordStartingWithPrefix_GivenEmptyString_ThenReturnsNothing() {
    let trie = Trie(["hello", "world", "helium"])
    XCTAssertEqual(trie.words(startingWith: ""), [])
  }
  
  func test_InitWithFile_AddsWordsByNewLine() {
    let trie = Trie("test.txt")
    
    XCTAssertEqual(trie.words, ["hello", "world"])
  }
}
