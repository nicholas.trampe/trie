//
//  Trie.swift
//  trie
//
//  Created by Nicholas Trampe on 9/12/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

private class TrieNode {
  let character: Character
  var count: Int = 0
  private(set) var children: [Character:TrieNode] = [:]
  
  init(_ c: Character) {
    character = c
  }
  
  // Public API
  
  public var isCompleteWord: Bool {
    get {
      return children["*"] != nil
    }
  }
  
  public var wordCount: Int {
    get {
      return children["*"]?.count ?? 0
    }
  }
  
  public var isTerminating: Bool {
    get {
      return children.count == 0
    }
  }
  
  public var sortedChildren: [(Character, TrieNode)] {
    get {
      return children.sorted(by: { (lhs, rhs) -> Bool in
        return lhs.value.count < rhs.value.count
      })
    }
  }
  
  @discardableResult
  public func add(_ character: Character) -> TrieNode {
    precondition(children[character] == nil, "Attempting to add an already existing character")
    let node = TrieNode(character)
    children[character] = node
    return node
  }
  
  public func contains(_ character: Character) -> TrieNode? {
    return children[character] 
  }
}

extension TrieNode: Hashable {
  static func == (lhs: TrieNode, rhs: TrieNode) -> Bool {
    return lhs.character == rhs.character
  }
  
  var hashValue: Int {
    get {
      return character.hashValue
    }
  }
}

class Trie {
  private var root = TrieNode("*")
  
  // Public API
  
  init(_ dictionary: [String]) {
    for word in dictionary {
      add(word)
    }
  }
  
  init(_ file: String) {
    let components = file.split(separator: ".")
    precondition(components.count == 2, "Invalid filename")
    
    let name = String(components[0])
    let ext = String(components[1])
    
    if let dictionaryPath = Bundle.main.path(forResource: name, ofType: ext) {
      let dictionaryURL = URL(fileURLWithPath: dictionaryPath)
      if let fileText = try? String(contentsOf: dictionaryURL) {
        let words = fileText.split(separator: "\n")
        for word in words {
          add(String(word))
        }
      }
    }
  }
  
  public var words: [String] {
    get {
      return words(startingAt: root, partialWord: "")
    }
  }
  
  public func words(startingWith prefix: String, limit: Int = Int.max, sortedByFrequency: Bool = false) -> [String] {
    if prefix.count == 0 {
      return []
    }
    
    if let node = getNode(for: prefix) {
      if sortedByFrequency {
        return frequentWords(startingAt: node, partialWord: prefix, limit: limit)
      } else {
        return words(startingAt: node, partialWord: prefix, limit: limit)
      }
    }
    
    return []
  }
  
  public func add(_ word: String) {
    var node: TrieNode = root
    
    for char in word {
      if let next = node.contains(char) {
        node = next
      } else {
        node = node.add(char)
      }
    }
    
    node.add("*")
  }
  
  public func contains(_ word: String) -> Bool {
    return getNode(for: word) != nil
  }
  
  public func increase(_ word: String) {
    var current: TrieNode = root
    
    for char in word {
      if let next = current.contains(char) {
        next.count += 1
        current = next
      } else {
        return
      }
    }
    
    if let term = current.contains("*") {
      term.count += 1
    }
  }
  
  // Private API
  
  private func getNode(for word: String) -> TrieNode? {
    var current: TrieNode = root
    
    for char in word {
      if let next = current.contains(char) {
        current = next
      } else {
        return nil
      }
    }
    
    return current
  }
  
  private func words(startingAt node: TrieNode, partialWord: String, limit: Int = Int.max) -> [String] {
    if node.isTerminating || limit <= 0 {
      return []
    }
    
    var result: [String] = []
    var numberOfWordsRemaining = limit
    
    if node.isCompleteWord {
      result.append(partialWord)
      numberOfWordsRemaining -= 1
    }
    
    for (character, node) in node.children {
      let word = partialWord + "\(character)"
      
      let newWords = words(startingAt: node, partialWord: word, limit: numberOfWordsRemaining)
      numberOfWordsRemaining -= newWords.count
      
      result += newWords
    }
    
    return result
  }
  
  private func frequentWords(startingAt start: TrieNode, partialWord: String, limit: Int = Int.max) -> [String] {
    var result: [String] = []
    var stack = Stack<(TrieNode, String)>()
    
    stack.push((start, partialWord))
    
    while let node = stack.pop(), result.count < limit {
      if node.0.isCompleteWord {
        if node.0.wordCount > 0 {
          result.insert(node.1, at: 0)
        } else {
          result.append(node.1)
        }
      }
      
      for (character, child) in node.0.sortedChildren {
        let word = node.1 + "\(character)"
        stack.push((child, word))
      }
    }
    
    return result
  }
}

extension Trie: CustomStringConvertible {
  var description: String {
    get {
      return words.reduce("", { $0 + "\($1)\n" })
    }
  }
}
