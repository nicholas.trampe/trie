//
//  PredictiveTextModel.swift
//  trie
//
//  Created by Nicholas Trampe on 9/12/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import Foundation

protocol PredictiveTextModelObserver: class {
  func didUpdate(_ text: String)
}

class PredictiveTextModel {
  weak var observer: PredictiveTextModelObserver? = nil
  private var dictionary = Trie("dictionary.txt")
  private var currentOperation: Operation? = nil
  private let queue = OperationQueue()
  
  init() {
    queue.maxConcurrentOperationCount = 1
  }
  
  func updateWords(after: String) {
    queue.cancelAllOperations()
    
    queue.addOperation { [weak self] in
      
      if let last = after.last, last == " " {
        self?.dictionary.increase(String(after.dropLast()))
      } 
      
      if let words = self?.dictionary.words(startingWith: after, limit: 25, sortedByFrequency: true) {
        let joined = words.reduce("", { $0 + "\($1)\n" })
        DispatchQueue.main.async {
          self?.observer?.didUpdate(joined)
        }
      }
    }
  }
}

