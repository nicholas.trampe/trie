//
//  ViewController.swift
//  trie
//
//  Created by Nicholas Trampe on 9/12/18.
//  Copyright © 2018 Nicholas Trampe. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  var model = PredictiveTextModel()
  @IBOutlet var textField: UITextField?
  @IBOutlet var textView: UITextView?

  override func viewDidLoad() {
    super.viewDidLoad()
    
    model.observer = self
  }

  @IBAction func textDidChange(field: UITextField) {
    if let text = field.text {
      model.updateWords(after: text)
    }
  }
}


extension ViewController: PredictiveTextModelObserver {
  func didUpdate(_ text: String) {
    self.textView?.text = text
  }
}
